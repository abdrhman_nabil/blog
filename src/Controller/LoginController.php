<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request,AuthenticationUtils $utils)
    {
        $errors=$utils->getLastAuthenticationError();
        $last_user_name=$utils->getLastUsername();
        return $this->render('login/index.html.twig', [
            'error' => $errors,
            'last_username'=>$last_user_name
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request,AuthenticationUtils $utils)
    {
        $errors=$utils->getLastAuthenticationError();
        $last_user_name=$utils->getLastUsername();
        return $this->render('login/index.html.twig', [
            'error' => $errors,
            'last_username'=>$last_user_name
        ]);
    }
}
