<?php
namespace App\Controller;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\User;
use App\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ArticleController extends Controller
{
/**
 *@Route("/",name="articles")
 * @Method({"GET"})
*/
    public function index(Request $request)
    {
         $articles=$this->getDoctrine()->getRepository(Article::class)->findBy(['is_published'=>1]);
        $categories=$this->getDoctrine()->getRepository(Category::class)->findAll();
        if($request->get('category_id'))
            $articles=$this->getDoctrine()->getRepository(Article::class)->findBy(['is_published'=>1,'category'=>$request->get('category_id')]);

        return $this->render('articles/published.html.twig',array('articles'=>$articles,'categories'=>$categories,'category_id'=>$request->get('category_id')));
    }


    /**
     *@Route("/articles",name="articles_list")
     * @Method({"GET"})
     */
    public function articles()
    {
        $articles=$this->getDoctrine()->getRepository(Article::class)->findAll();

        return $this->render('articles/index.html.twig',array('articles'=>$articles));
    }

    /**
     *@Route("/comments",name="add_comment")
     * @Method({"GET"})
     */
    public function add_comment(Request $request)
    {
        $userId =  $this->getUser()->getId();
        $comment=new Comment();
        $comment->setComment($request->get('comment'));
        $comment->setArticle($this->getDoctrine()->getRepository(Article::class)->find($request->get('article_id')));
        $comment->setUser($this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId()));
        $entityManger=$this->getDoctrine()->getManager();
        $entityManger->persist($comment);
        $entityManger->flush();
        return $this->redirectToRoute('articles');
    }



    /**
     *@Route("/articles/create")
     * @Method{{"GET","POST"}}
     */
    public function create(Request $request)
    {
        $article=new Article();

        $form=$this->createFormBuilder($article)
            ->add('title',TextType::class,['attr'=>['class'=>'form-control']])
            ->add('body',TextareaType::class,['attr'=>['class'=>'form-control','required'=>false]])
            ->add('category', EntityType::class, array(
                'choice_label' => function ($category) {
                    return $category->getId() . ' ' . $category->getName();
                },
    'class' => 'App\Entity\Category',
    'query_builder' => function (CategoryRepository $er) {
        return $er->createQueryBuilder('c')
            ->orderBy('c.id', 'ASC');
    },
'attr'=>['class'=>'form-control']))
            ->add('Save',SubmitType::class,['attr'=>['class'=>'btn btn-primary mt-3','label'=>'Save']])->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
          $article= $form->getData();
          $article->setIsPublished(0);
          $entityManger=$this->getDoctrine()->getManager();
          $entityManger->persist($article);
          $entityManger->flush();
          return $this->redirectToRoute('articles_list');
        }
        return $this->render('articles/create.html.twig',['form'=>$form->createView()]);

    }

    /**
     *@Route("/articles/{id}")
     */

    public function show($id)
    {
        $article=$this->getDoctrine()->getRepository(Article::class)->find($id);

        return $this->render('articles/show.html.twig',array('article'=>$article));
    }
    /**
     *@Route("/articles/{id}/publish")
     */

    public function publish($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article=$entityManager->getRepository(Article::class)->find($id);
        $article->setIsPublished(1);
        $entityManager->flush();
        return $this->redirectToRoute('articles_list');
    }

    /**
     *@Route("/articles/{id}/unpublish")
     */

    public function Unpublish($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article=$entityManager->getRepository(Article::class)->find($id);
        $article->setIsPublished(0);
        $entityManager->flush();
        return $this->redirectToRoute('articles_list');
    }


    /**
     *@Route("/article/category")
     */

    public function category()
    {
        $entityManger=$this->getDoctrine()->getManager();

        $article=new Article();
        $article->setTitle('test title');
        $article->setBody('test Body');
        $article->setIsPublished(1);

        $category=new Category();
        $category->setName('test Cat');

        $article->setCategory($category);
        $entityManger->persist($category);
        $entityManger->persist($article);
        $entityManger->flush();

        return new Response('great');
    }
//
//    /**
//     *@Route("/articles/save")
//     */
//    public function save()
//    {
//      $entityManger=$this->getDoctrine()->getManager();
//      $article= new Article();
//      $article->setTitle('article 2 title');
//      $article->setBody('the body of article 2');
//
//      $entityManger->persist($article);
//      $entityManger->flush();
//      return new Response('saves article with id of '.$article->getId());
//    }




}
