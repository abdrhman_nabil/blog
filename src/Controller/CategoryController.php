<?php
namespace App\Controller;
use App\Entity\Category;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CategoryController extends Controller
{
    /**
     *@Route("/categories/list",name="category_list")
     * @Method({"GET"})
     */
    public function index()
    {
        $categories=$this->getDoctrine()->getRepository(Category::class)->findAll();

        return $this->render('category/index.html.twig',array('categories'=>$categories));
    }



    /**
     *@Route("/categories/create")
     * @Method{{"GET","POST"}}
     */
    public function create(Request $request)
    {
        $category=new Category();
        $form=$this->createFormBuilder($category)->add('name',TextType::class,['attr'=>['class'=>'form-control']])
            ->add('Save',SubmitType::class,['attr'=>['class'=>'btn btn-primary mt-3','label'=>'Save']])->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $category= $form->getData();
            $entityManger=$this->getDoctrine()->getManager();
            $entityManger->persist($category);
            $entityManger->flush();
            return $this->redirectToRoute('articles_list');
        }
        return $this->render('category/create.html.twig',['form'=>$form->createView()]);

    }


    /**
     *@Route("/category/{id}")
     */
    public function show($id)
    {
        $category=$this->getDoctrine()->getRepository(Category::class)->find($id);

        return $this->render('category/show.html.twig',array('category'=>$category));
    }

//
//    /**
//     *@Route("/category/save")
//     */
//    public function save()
//    {
//      $entityManger=$this->getDoctrine()->getManager();
//      $category= new Category();
//      $category->setName('Cat 4');
//      $entityManger->persist($category);
//      $entityManger->flush();
//      return new Response('saves category with id of '.$category->getId());
//    }
//



}
