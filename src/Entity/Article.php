<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text",length=110)
     */
    private $title;


    /**
     * @ORM\Column(type="text")
     */

    private $body;

    /**
     * @ORM\Column(type="boolean", options={"default":0})
     */

    private $is_published;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;


    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id",referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="article")
     */
    private $comments;

    private $category_id;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setTitle($title)
    {
        $this->title=$title;
    }

    public function setBody($body)
    {
         $this->body=$body;
    }


    public function getIs_published()
    {
        return $this->is_published;
    }

    public function setCategory(Category $category=null)
    {
        $this->category=$category;
    }


    public function getCategory()
    {
        return $this->category;
    }



    public function setIsPublished($is_published)
    {
        $this->is_published=$is_published;
    }

}
