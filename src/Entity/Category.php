<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="text",length=110)
     */

    private $name;


    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    private $articles;

    public function __construct()
    {
       $this->created_at= new \DateTime('now');
       $this->articles=new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getArticles()
    {
        return $this->articles;
    }

    public function setName($name)
    {
        $this->name=$name;
    }


}
