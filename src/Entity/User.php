<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface ,\Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="text",length=110)
     */

    private $username;



    /**
     * @ORM\Column(type="boolean")
     */

    private $is_admin;


    /**
     * @ORM\Column(type="string",length=191)
     */
private $email;
    /**
     * @ORM\Column(type="string",length=255)
     */
private $password;


    public function __construct()
    {
      $this->created_at=new \DateTime('now');
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($name)
    {
        $this->username=$name;
    }



    public function getIs_admin()
    {
        return $this->is_admin;
    }

    public function setIs_admin($is_admin)
    {
        $this->is_admin=$is_admin;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email=$email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password=$password;
    }

    public function getRoles()
    {
     return[
         'ROLE_USER'
     ];
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->email,
            $this->password,

        ]);
    }

    public function unserialize($string)
    {
        list(
            $this->id,
            $this->username,
            $this->email,
            $this->password,

        )=unserialize($string,['allowed_classes'=>false]);
    }


}
