# blog

#routes 
/*
*@Route("/",name="articles") get a list of all published articles
*@Route("/articles",name="articles_list") get a list of all articles
*@Route("/comments",name="add_comment") list comments of user for an article
*@Route("/articles/create") create an article
*@Route("/articles/{id}")/ show a single article 
*@Route("/articles/{id}/publish") publish an article
*@Route("/articles/{id}/unpublish") unpublish an article
*@Route("/categories/list",name="category_list") list all categories
*@Route("/categories/create") create a category
*@Route("/category/{id}") show a single category  
*@Route("/login", name="login") login
*@Route("/logout", name="logout") logout
*/

#commands to run 
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate 
//to migrate the DB

php bin/console server:start to run the localserver 
php bin/console doctrine:fixtures:load to load fixture
php bin/console make:entity to make entities




